# Raspberry Pi Stock Ticker

## Setup Instructions

Install dietpi normally

```bash
    git clone https://github.com/hzeller/rpi-rgb-led-matrix.git
    cd ./rpi-rgb-led-matrix/
    sudo apt-get update && sudo apt-get install python3-dev git pip build-essential -y
    make build-python PYTHON=python3
    sudo make install-python PYTHON=python3
    cd ./bindings/python/samples/
    wget https://gitlab.com/snippets/1874042/raw -O stock.py
    pip3 install requests==2.22.0
    sudo python3 ./stock.py
```
