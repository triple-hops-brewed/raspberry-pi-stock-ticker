#%%
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np

#%%
matrixSize = (32, 64, 3)
#%%
a = np.random.randint(1, high=256, size=(32, 128, 3))

# for i in range(10):
#     a = np.delete(a,0,1)
#     a.shape

#%%
plt.imshow(a[:, :64])
plt.show()

#%%
blank = np.zeros(matrixSize, dtype=int)
noise = np.random.randint(1, high=256, size=(32, 128, 3))

#%%
buffer = np.concatenate((blank, noise), axis=1)


#%%
plt.imshow(buffer[:, :64])
plt.show()


#%%
fig = plt.figure(figsize=matrixSize[0:2], facecolor="black")


def update():
    np.delete(buffer, 0, 1)


plt.imshow(buffer[:, :64])
ani = FuncAnimation(fig, update, frames=buffer.shape[1])
plt.show()


#%%
